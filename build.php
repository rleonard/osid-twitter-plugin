<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of the Twitter Plugin for the OSID Server.
	 *
	 * the Twitter Plugin for the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the Twitter Plugin for the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the Twitter Plugin for the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	function build($db, $SETTINGS, $build_reason = 'cron', $id = null) {
		$build = function($id, $db, $SETTINGS, $build_reason = 'cron') {
			$item = $db->querySingle("SELECT * FROM item_entries WHERE id='$id'", true);
			$configFile = $SETTINGS["pluginDirectory"]."us.ryanleonard.osid.twitter/data/config.json";
			$config = json_decode(file_get_contents($configFile), true);
			//$config = json_decode("data/config.json", true);
			require_once("lib/codebird/src/codebird.php");
			\Codebird\Codebird::setConsumerKey($config["apiKey"], $config["apiSecret"]);
			$twitter = \Codebird\Codebird::getInstance();
			$tweets = json_decode($item["param_object"], true);
			foreach ($tweets as $tweetID => $tweet) {
				$tweeters = array_unique(array_filter($tweet["tweeters"]));
				$retweeters = array_unique(array_filter($tweet["retweeters"]));
				$twitter->setToken($config["accounts"][$tweeters[0]]["token"], $config["accounts"][$tweeters[0]]["secret"]);
				$initialStatus = $twitter->statuses_update('status='.$tweet["message"]);
				foreach($retweeters as $retweeter) {
					$twitter->setToken($config["accounts"][$retweeter]["token"], $config["accounts"][$retweeter]["secret"]);
					$twitter->statuses_retweet('id='.$initialStatus["id_str"]);
				}
				for ($account=1; $account < count($tweeters); $account++) { 
					$tweeter = $tweeters[$account];
					$twitter->setToken($config["accounts"][$tweeter]["token"], $config["accounts"][$tweeter]["secret"]);
					$initialStatus = $twitter->statuses_update('status='.$tweet["message"]);
				}
			}
		};
		if(isset($id)) {
			$build($id, $db, $SETTINGS, $build_reason);
		}
	}