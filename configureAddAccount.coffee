###
 # Copyright 2013-2014 Ryan Leonard.
 # This file is part of the Twitter Plugin for the OSID Server.
 #
 # the Twitter Plugin for the OSID Server is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # the Twitter Plugin for the OSID Server is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with the Twitter Plugin for the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
###
# Handle the "add account" form
configureAddAccount = ->
	$("#newAccount").submit (e) ->
		e.preventDefault()
		$.post '/plugin/?/us.ryanleonard.osid.twitter/newAccount/', $('#newAccount').serialize()
		false
	$("#apiKeys").submit (e) ->
		e.preventDefault()
		$.post '/plugin/?/us.ryanleonard.osid.twitter/apiKeys/', $("#apiKeys").serialize()
		false

configureAddAccount()
