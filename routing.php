<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of the Twitter Plugin for the OSID Server.
	 *
	 * the Twitter Plugin for the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the Twitter Plugin for the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the Twitter Plugin for the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	function doAuth($noAuthURL) {
		global $root;
		include_once($root."php/auth.php");
		$user = auth($noAuthURL);
	}
	function getConfig() {
		global $SETTINGS;
		$configFile = $SETTINGS["pluginDirectory"]."us.ryanleonard.osid.twitter/data/config.json";
		$config = json_decode(file_get_contents($configFile), true);
		return $config;
	}
	function setConfig($config) {
		global $SETTINGS;
		$configFile = $SETTINGS["pluginDirectory"]."us.ryanleonard.osid.twitter/data/config.json";
		file_put_contents($configFile, json_encode($config));
	}
	function template($title, $body, $bodyScripts = "") {
		echo <<<EOD
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>{$title} | OSID Publishing Server</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/lib/font-awesome/css/font-awesome.min.css">
	</head>
	<body>
		<div class="container">
			$body
		</div>
		<script src="/lib/jquery-1.10.2.min.js"></script>
		<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
		$bodyScripts
	</body>
</html>
EOD;
	}
	require "lib/flight/Flight.php";
	Flight::route("POST /\?/us.ryanleonard.osid.twitter/newAccount/", function() {
		global $SETTINGS;
		doAuth("/admin/login/");
		$handle = $_POST["handle"];
		if(substr($handle, 0, 1) === "@") {
			$handle = substr($handle, 1);
		}
		$allowPosting = isset($_POST["allowPosting"]) && $_POST["allowPosting"];
		$allowRetweeting = isset($_POST["allowRetweeting"]) && $_POST["allowRetweeting"];
		$user = array(
			"handle" => $handle,
			"auth" => false,
			"authLastCheck" => time(),
			"allowPosting" => $allowPosting,
			"allowRetweeting" => $allowRetweeting
		);
		$config = getConfig();
		$config["accounts"][$handle] = $user;
		setConfig($config);
	});

	Flight::route("POST /\?/us.ryanleonard.osid.twitter/apiKeys/", function() {
		global $SETTINGS;
		doAuth("/admin/login/");
		$key = $_POST["apiKey"];
		$secret = $_POST["apiSecret"];
		$config = getConfig();
		$config["apiKey"] = $key;
		$config["apiSecret"] = $secret;
		setConfig($config);
	});

	Flight::route("/\?/us.ryanleonard.osid.twitter/toggle/allowPosting/@account/", function($account) {
		doAuth("/admin/login/");
		$newVal = $_POST["value"];
		$newVal = $newVal === "true" || $newVal === true ? true : false;
		$config = getConfig();
		$config["accounts"][$account]["allowPosting"] = $newVal;
		setConfig($config);
	});

	Flight::route("/\?/us.ryanleonard.osid.twitter/toggle/allowRetweeting/@account/", function($account) {
		doAuth("/admin/login/");
		$newVal = $_POST["value"];
		$newVal = $newVal === "true" || $newVal === true ? true : false;
		$config = getConfig();
		$config["accounts"][$account]["allowRetweeting"] = $newVal;
		setConfig($config);
	});

	Flight::route("/\?/us.ryanleonard.osid.twitter/authAccount/@account/", function($account) {
		global $SETTINGS;
		$safeAccount = htmlentities($account);
		$fullURL = $SETTINGS["webRoot"]."plugin/?/us.ryanleonard.osid.twitter/authAccount/$safeAccount/";
		$body = <<<EOD
<h3>Twitter Account Authentication</h3>
<p>
	By adding a Twitter account, tweets and re-tweets can be sent automatically.
	Permission needs to be granted for this app access the account.
</p>
<p>
	If you have access to the @{$safeAccount} Twitter account,
	you can click the button below to allow us to have access to your account.
<p>
<p>
	If you do not have access to this account, you can send this URL to someone who does.
	Anyone can authenticate a Twitter account if they have access to the account,
	they do not need to have an account on this website.<br />
	{$fullURL}
</p>
<p class="text-center">
	<a href="{$fullURL}continue/" class="btn btn-primary">
		<i class="fa fa-twitter"></i> 
		Continue to Twitter Authentication
	</a>
</p>
EOD;
		template("Authenticate Account", $body);
	});

	Flight::route("/\?/us.ryanleonard.osid.twitter/authAccount/@account/continue/", function($account) {
		global $SETTINGS;
		$config = getConfig();
		// Store account name in a temporary variable to retrieve
		// after authentication
		session_start();
		$_SESSION["tempAccount"] = $account;
		// Setup codebird library for Twitter
		require_once("lib/codebird/src/codebird.php");
		\Codebird\Codebird::setConsumerKey($config["apiKey"], $config["apiSecret"]);
		$twitter = \Codebird\Codebird::getInstance();
		// Setup callback and redirect the user to
		// the Twitter authentication
		$callback = $SETTINGS["webRoot"]."plugin/?/us.ryanleonard.osid.twitter/authAccount/$account/callback/";
		$reply = $twitter->oauth_requestToken(array(
			"oauth_callback" => $callback
		));
		$twitter->setToken($reply->oauth_token, $reply->oauth_token_secret);
		$_SESSION['oauth_token'] = $reply->oauth_token;
		$_SESSION['oauth_token_secret'] = $reply->oauth_token_secret;
		$_SESSION['oauth_verify'] = true;
		$auth_url = $twitter->oauth_authorize();
		Flight::redirect($auth_url, 303);
	});

	//Flight::route("/\?/us.ryanleonard.osid.twitter/authAccount/@account/callback/&oauth_token=@token:([^&]*)&oauth_verifier=@verifier:(.*)", function($account, $token, $verifier) {
	Flight::route("/\?/us.ryanleonard.osid.twitter/authAccount/@account/callback/&oauth_token=@token&oauth_verifier=@verifier", function($account, $token, $verifier) {
		global $SETTINGS;
		session_start();
		$config = getConfig();
		require_once("lib/codebird/src/codebird.php");
		\Codebird\Codebird::setConsumerKey($config["apiKey"], $config["apiSecret"]);
		$twitter = \Codebird\Codebird::getInstance();
		$callback = $SETTINGS["webRoot"]."plugin/?/us.ryanleonard.osid.twitter/authAccount/$account/callback/";
		$twitter->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
		$reply = $twitter->oauth_accessToken(array(
			"oauth_verifier" => $verifier
		));
		if($account != $reply->screen_name) {
			die("Sorry, the account currently logged in (" . htmlentities($reply->screen_name) . ") doesn't match " . htmlentities($account));
		}
		$userToken = $reply->oauth_token;
		$userTokenSecret = $reply->oauth_token_secret;
		$config["accounts"][$account]["uid"] = $reply->user_id;
		$config["accounts"][$account]["token"] = $userToken;
		$config["accounts"][$account]["secret"] = $userTokenSecret;
		$config["accounts"][$account]["auth"] = true;
		$config["accounts"][$account]["authLastCheck"] = time();
		setConfig($config);
		$fullURL = $SETTINGS["webRoot"];
		$body = <<<EOD
<h3>Account authenticated</h3>
<p>
	Your account has been authenticated.
</p>
<p>
	<a href="{$fullURL}" class="btn btn-default">Home</a>
	<a href="javascript:window.close();" class="btn btn-primary">Close Window</a>
<p>
EOD;
		template("Account Authenticated", $body);
	});

	Flight::start();