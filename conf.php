<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of the Twitter Plugin for the OSID Server.
	 *
	 * the Twitter Plugin for the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the Twitter Plugin for the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the Twitter Plugin for the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	if(!isset($PLUGINS)) {
		$PLUGINS = array();
	}
	$PLUGINS["us.ryanleonard.osid.twitter"] = array(
		"id"=>"us.ryanleonard.osid.twitter",
		"name"=>"Twitter Posts",
		"description"=>"Allows posting messages to Twitter accounts.",
		"config"=>"configure.php",
		"configSave"=>"configureSave.php",
		"screenshots"=>array(
			/*
			array(
				"file"="screenshot.png",
				"title"="Awesome screenshot!",
				"caption"="Look at what I can do!"
			)
			*/
		),
		"types"=>array(
			"us.ryanleonard.osid.twitter.twitterPost" => array(
				"name" => "us.ryanleonard.osid.twitter.twitterPost",
				"description" => "Post a message on one or more Twitter accounts.",
				"plugin_id" => "us.ryanleonard.osid.twitter",
				"display_name" => "Upcomming Event",
				"form_file" => "twitterPost.php",
				"create_function" => "createTweet.php",
				"build_function" => "build.php",
				"allow_public" =>  'true',
				"screenshot_json" =>  array(),
				"param_object" =>  array()
			)
		),
	);