<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of the Twitter Plugin for the OSID Server.
	 *
	 * the Twitter Plugin for the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the Twitter Plugin for the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the Twitter Plugin for the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	$configFile = $SETTINGS["pluginDirectory"]."us.ryanleonard.osid.twitter/data/config.json";
	$config = json_decode(file_get_contents($configFile), true);
	$accounts = <<<EOD
<table class="table">
	<thead>
		<tr>
			<th>Twitter Handle</th>
			<th>Authenticated</th>
			<th>Allow Posting</th>
			<th>Allow Re-Tweeting</th>
			<th>Remove</th>
		</tr>
	</thead>
	<tbody>
EOD;
	foreach ($config["accounts"] as $username => $account) {
		$handle = $account["handle"];
		$auth = $account["auth"];
		$authStr = $auth ? "Yes" : "No";
		$authLink = !($auth) ? "<a href='/plugin/?/us.ryanleonard.osid.twitter/authAccount/$handle/' target='_blank'><i class='fa fa-link'></i> Authenticate Account</a>" : "";
		$authLastCheck = $account["authLastCheck"];
		$lastCheckDate = date("c", $authLastCheck);
		$lastCheckEnglish = date("r", $authLastCheck);
		$allowPost = $account["allowPosting"] ? "true" : "false";
		$allowTweet = $account["allowRetweeting"] ? "true" : "false";
		$accounts .= <<<EOD
		<tr>
			<td>$handle</td>
			<td>
				$authStr
				(Last checked <abbr class="timeago" title="$lastCheckDate">$lastCheckEnglish></abbr>)
				<a href="/plugin/?/us.ryanleonard.osid.twitter/checkAuthentication/$handle/" data-ajax-link><i class="fa fa-refresh"></i></a>
				$authLink
			</td>
			<td>
				<abbr title="Click to toggle if this account can post new tweets.">
					<a
						href="/plugin/?/us.ryanleonard.osid.twitter/toggle/allowPosting/$handle/"
						data-ajax-toggle
						data-true="Yes" data-false="No"
						data-value="$allowPost">
						$allowPost
					</a>
				</abbr>
			</td>
			<td>
				<abbr title="Click to toggle if this account can retweet.">
					<a
						href="/plugin/?/us.ryanleonard.osid.twitter/toggle/allowRetweeting/$handle/"
						data-ajax-toggle
						data-true="Yes" data-false="No"
						data-value="$allowTweet">
						$allowTweet
					</a>
				</abbr>
			</td>
			<td>
				<a
					href="/plugin/?/us.ryanleonard.osid.twitter/removeAccount/$handle/"
					class="btn btn-default"
					data-ajax-button data-ajax-confirm
					data-ajax-confirmText="Really delete?"
					data-ajax-confirmClass="btn-danger"
					data-ajax-deleteRow>
					Delete Account...
				</a>
			</td>
		</tr>
EOD;
	}
	$accounts .= <<<EOD
	</tbody>
</table>
EOD;

	$apiKey = isset($config["apiKey"]) ? $config["apiKey"] : "";
	$apiSecret = isset($config["apiSecret"]) ? substr($config["apiSecret"], 0, 5)."..." : "";

	$form = <<<EOD
<p>
	The Twitter plugin allows messages to be posted on accounts, and optionally retweeted by other accounts.
</p>
<p>
	A Twitter "Application" needs to be created so Twitter can track what messages are sent
	through this website.  Please complete the steps below:
</p>
<ol>
	<li>
		Visit <a href="https://apps.twitter.com/">https://apps.twitter.com/</a>,
		and sign in with a Twitter account
	</li>
	<li>
		Create a new application at
		<a href="https://apps.twitter.com/app/new">https://apps.twitter.com/app/new</a>,
		giving an appropriate name, description, and website, and entering a placeholder value for the "Callback URL".<br />
		We recommend a name like "&lt;School District&gt; Tweets" or "&lt;School District&gt; Automated Tweeter",
		replacing "&lt;School District&gt;" with the acronymn of the school or school district.
		This name will be attached to all tweets sent through this plugin.<br />
		We recommend mentioning that this is an official application for the school
		or district in the description, which will only be visible when authenticating district
		Twitter accounts to automatically send messages.<br />
		The Website URL will be attached to each tweet sent with this program.
		We recommend setting it to the homepage of this application, or to the
		school or district homepage.<br />
		You also need to fill out the Callback URL with a placeholder value.
		We suggest using a URL that you own, such as the homepage for your district,
		in case the URL is accidentally used.
	</li>
	<li>
		Twitter requires you to add your mobile number to the account that
		was signed into during the first step.  Please follow the steps in
		<a href="https://support.twitter.com/articles/110250-adding-your-mobile-number-to-your-account-via-web">
			Twitter's guide
		</a>.
	<li>
		Once you have created the application and added your mobile number,
		please modify the permissions, changing them to "Read and Write",
		and clicking "Update Settings".
	</li>
	<li>
		Switch back into the "API Keys" tab of the settings,
		and copy down the API key and secret into the fields below.<br />
		Please note that only the first few characters of the API secret are shown
		when you come back to this page for added security.
	</li>
</ol>
<form role="form" class="form-inline" id="apiKeys" action="/plugin/?/us.ryanleonard.osid.twitter/apiKeys/" method="POST">
	<div class="form-group">
		<label class="sr-only" for="apiKey">API Key</label>
		<input type="text" class="form-control" id="apiKey" name="apiKey" placeholder="API Key" value="$apiKey" />
	</div>
	<div class="form-group">
		<label class="sr-only" for="apiSecret">API Secret</label>
		<input type="text" class="form-control" id="apiSecret" name="apiSecret" placeholder="API Secret" value="$apiSecret" />
	</div>
	<button type="submit" class="btn btn-default">Save API Keys</button>
</form>
<p>
	For automatic posting to be possible,
	the plugin needs to have access to the
	Twitter user accounts that will be used.
	The table below shows the accounts that
	have been added and their current configuration.
	In he form below, additional users can be added. 
</p>
$accounts
<form role="form" class="form-inline" id="newAccount" action="/plugin/?/us.ryanleonard.osid.twitter/newAccount/" method="POST">
	<div class="form-group">
		<label for="handle">Handle</label>
		<input type="text" class="form-control" id="handle" name="handle" placeholder="@CodeLenny">
		<div class="help-block">
			Enter the Twitter handle, with or without "@".
		</div>
	</div>
	<div class="checkbox">
		<label>
			<input type="checkbox" id="allowPosting" name="allowPosting" value="true" checked />
			Allow posting <span class="text-muted">(this can be changed later)</span>
		</label>
	</div>
	<div class="checkbox">
		<label>
			<input type="checkbox" id="allowRetweeting" name="allowRetweeting" value="true" />
			Allow retweeting <span class="text-muted">(this can be changed later)</span>
		</label>
	</div>
	<button type="submit" class="btn btn-primary">Add Account</button>
</form>
EOD;
	
	$bodyScripts = <<<EOD
	<script src="/plugin/?plugin=us.ryanleonard.osid.twitter&file=ajaxLinks.js"></script>
	<script src="/plugin/?plugin=us.ryanleonard.osid.twitter&file=configureAddAccount.js"></script>
	<script src="/lib/timeago.js"></script>
	<script>
		jQuery.timeago.settings.allowFuture = true;
		jQuery(document).ready(function() {
			jQuery("abbr.timeago").timeago();
		});
	</script>
EOD;

	$encloseForm = false;