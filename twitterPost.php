<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of the Twitter Plugin for the OSID Server.
	 *
	 * the Twitter Plugin for the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the Twitter Plugin for the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the Twitter Plugin for the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	$configFile = $SETTINGS["pluginDirectory"]."us.ryanleonard.osid.twitter/data/config.json";
	$config = json_decode(file_get_contents($configFile), true);
	$tweetAccounts = array();
	$retweetAccounts = array();
	foreach ($config["accounts"] as $accountName => $accountConfig) {
		if($accountConfig["allowPosting"]) {
			$tweetAccounts[] = $accountName;
		}
		if($accountConfig["allowRetweeting"]) {
			$retweetAccounts[] = $accountName;
		}
	}
	$tweetAccounts = json_encode($tweetAccounts);
	$retweetAccounts = json_encode($retweetAccounts);
	$form = <<<EOD
<style>
	.scrollable .tt-dropdown-menu {
		max-height: 150px;
		overflow-y: auto;
	}
</style>
<input type="hidden" name="serializedData" id="serializedData" value="" />
<div class="form-group">
	<label for="defaultText">Default Tweet Text</label>
	<textarea class="form-control" rows="2" name="defaultText" id="defaultText" placeholder="Please dropoff Sports Medical Forms in the Nurse's office."></textarea>
	<span class="glyphicon glyphicon-ok form-control-feedback"></span>
	<div class="help-block">
		The default text to tweet.  The message can be customized for each account selected below.
	</div>
</div>
<div id="lastGroup"></div>
<div class="hide group" id="templateGroup">
	<div class="tweet-group form-group">
		<label>User</label>
		<div class="scrollable input-group">
			<span class="input-group-addon">@</span>
			<input type="text" class="typeahead form-control" name="username[]" />
			<span class="input-group-btn">
				<button class="btn btn-primary addUsername" type="button">Add</button>
			</span>
		</div>
		<div class="help-block"></div>
	</div>
	<div class="message-group form-group">
		<label>Message</label>
		<textarea class="form-control" rows="2" name="text[]" placeholder="Please dropoff Sports Medical Forms in the Nurse's office."></textarea>
		<span class="glyphicon glyphicon-ok form-control-feedback"></span>
		<div class="help-block"></div>
	</div>
	<div class="retweet-group form-group">
		<label>Retweet</label>
		<div class="scrollable input-group input-group-sm">
			<span class="input-group-addon">@</span>
			<input type="text" class="typeahead form-control" name="retweet[]" />
			<span class="input-group-btn">
				<button class="btn btn-primary addRetweet" type="button">Add</button>
			</span>
		</div>
		<div class="help-block"></div>
	</div>
</div>
<p>
	<button type="buton" class="btn btn-default addTemplateGroup">Create Another Tweet</button>
	<!--<button type="button" class="btn btn-primary submitTweets">Submit Tweets</button>-->
</p>
EOD;
	$scriptAppend = <<<EOD
<script>
	var tweetAccounts = JSON.parse('{$tweetAccounts}');
	var retweetAccounts = JSON.parse('{$retweetAccounts}');
</script>
<script src="/plugin/?plugin=us.ryanleonard.osid.twitter&file=typeahead.js"></script>
<script src="/plugin/?plugin=us.ryanleonard.osid.twitter&file=newTweet.js"></script>
EOD;
