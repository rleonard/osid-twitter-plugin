###
 # Copyright 2013-2014 Ryan Leonard.
 # This file is part of the Twitter Plugin for the OSID Server.
 #
 # the Twitter Plugin for the OSID Server is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # the Twitter Plugin for the OSID Server is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with the Twitter Plugin for the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
###
ajaxLinks = ->
	$("[data-ajax-link]").click (e) ->
		e.preventDefault()
		url = $(@).attr "href"
		$.get url, (response) ->
			#if response.text?
				# alert
		false
	$("[data-ajax-toggle]").each (index, el) ->
		$(el).text $(el).data "" + $(el).data "value"
		$(el).click (e) ->
			e.preventDefault()
			newVal = if "" + $(@).data("value") is "true" then "false" else "true"
			$(@).text "Toggling to #{newVal}..."
			$.post $(@).attr("href"),
				value: newVal
			, (response) ->
				$(el).data "value", newVal
				$(el).text $(el).data newVal
			false
	$("[data-ajax-confirm]").click (e) ->
		e.preventDefault()
		if $(@).data "confirming"
			url = $(@).attr "href"
			$.get url, (response) ->
				#if response.text?
					# alert
			if $(@).data "ajax-deleteRow"
				$(@).parents("tr").remove()
		else
			$(@).addClass $(@).data "confirmClass"
			$(@).text $(@).data "confirmText"
			$(@).data "confirming", true
		false

ajaxLinks()