<?php
	/*
	 * Copyright 2013-2014 Ryan Leonard.
	 * This file is part of the Twitter Plugin for the OSID Server.
	 *
	 * the Twitter Plugin for the OSID Server is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * the Twitter Plugin for the OSID Server is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with the Twitter Plugin for the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
	 */
	function create($SETTINGS) {
		$tweets = json_decode($_POST["serializedData"], true);
		// TODO: validate
		$desc = "";
		foreach ($tweets as $tweetId => $tweet) {
			//$tweeters = split(" ", $tweet["tweeters"]);
			$tweeters = $tweet["tweeters"];
			$message = $tweet["message"];
			//$retweeters = split(" ", $tweet["retweeters"]);
			$retweeters = $tweet["retweeters"];
			$tweeterString = implode(", ", array_unique(array_filter($tweeters)));
			$desc .= "'{$message}' will be tweeted by {$tweeterString}";
			if(count(array_filter($retweeters)) > 0) {
				$retweeterString = implode(", ", array_unique(array_filter($retweeters)));
				$desc .= ". {$tweeters[0]}'s message will be retweeted by {$retweeterString}";
			}
			$desc .= ".\n";
		}
		return array(
			"name" => substr($tweets[0]["message"], 0, 40)."...",
			"description" => $desc,
			"param_object" => $_POST["serializedData"],
			"start_time"=>time(), // TODO: add times to config
			"target_time"=>time()
		);
	}