###
 # Copyright 2013-2014 Ryan Leonard.
 # This file is part of the Twitter Plugin for the OSID Server.
 #
 # the Twitter Plugin for the OSID Server is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # the Twitter Plugin for the OSID Server is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with the Twitter Plugin for the OSID Server.  If not, see <http://www.gnu.org/licenses/>.
###
# substringMatcher borrowed from examples:
# https://gist.github.com/jharding/9458744#file-the-basics-js
substringMatcher = (strs) ->
	(q, cb) ->
		matches = []
		substrRegex = new RegExp(q, 'i')
		$.each strs, (i, str) ->
			if substrRegex.test str
				matches.push
					value: str
		cb matches

counter = (target) ->
	target.on 'keyup', ->
		len = $(@).val().length
		if len > 140
			textClass = "text-danger"
			formClass = "has-error"
			iconClass = "glyphicon-remove"
		else if len > 100
			textClass = "text-warning"
			formClass = "has-warning"
			iconClass = "glyphicon-warning-sign"
		else
			textClass = ""
			formClass = ""
			iconClass = "glyphicon-ok"
		$(@).siblings(".help-block").text("#{len}/140").removeClass("text-warning text-danger").addClass textClass
		$(@).siblings("span.glyphicon").removeClass("glyphicon-ok glyphicon-warning-sign glyphicon-remove").addClass iconClass
		$(@).parents(".form-group").removeClass("has-warning has-error").addClass formClass

$("#defaultText").on 'keyup', ->
	val = $(@).val()
	if val is ""
		val = "Please dropoff Sports Medical Forms in the Nurse's office."
	$("[name='text[]']").attr 'placeholder', val
counter $ "#defaultText"

typeaheadTemplate =
	empty: """
		<div class="emptyMessage">
			No configured users match your search.
		</div>
	"""
	header: """
		<div class="dropdown">
			<ul class="dropdown-menu">
	"""
	footer: """
			</ul>
		</div>
	"""
	suggestion: (data) ->
		"""
			<li role="presentation"><a href="#">#{data.value}</a></li>
		"""


newTweet = ->
	clone = $("#templateGroup").clone()
	clone.attr "id", ""
	clone.find("input[name='username[]']").typeahead
		highlight: true
	,
		name: "TwitterUsers"
		source: substringMatcher tweetAccounts
		templates: typeaheadTemplate
	clone.find("input[name='retweet[]']").typeahead
		highlight: true
	,
		name: "TwitterUsers"
		source: substringMatcher retweetAccounts
		templates: typeaheadTemplate
	clone.find("button.addUsername").on 'click', ->
		parent = $(@).parents(".form-group")
		ta = parent.find("input[name='username[]']")
		val = ta.typeahead 'val'
		ta.typeahead 'val', ""
		if !(parent.data('tweeters')?)
			parent.data 'tweeters', []
		parent.data('tweeters').push val
		str = parent.data('tweeters').join(", ")
		parent.find(".help-block").text str
	clone.find("button.addRetweet").on 'click', ->
		parent = $(@).parents(".form-group")
		ta = parent.find("input[name='retweet[]']")
		val = ta.typeahead 'val'
		ta.typeahead 'val', ""
		if !(parent.data('retweeters')?)
			parent.data 'retweeters', []
		parent.data('retweeters').push val
		str = parent.data('retweeters').join(", ")
		parent.find(".help-block").text str
	counter clone.find "textarea"
	clone.removeClass "hide"
	clone.insertBefore $ "#lastGroup"

newTweet()

$(".addTemplateGroup").click (e) ->
	e.preventDefault()
	newTweet()
	false

#$(".submitTweets").click (e) ->
$("form.container").submit (e) ->
	#e.preventDefault()
	defaultText = $("#defaultText").val()
	tweets = []
	$(".group:not(.hide)").each (index, el) ->
		tweeters = $(el).find(".tweet-group").data("tweeters") ? []
		tweeterVal = $(el).find("input[name='username[]']").typeahead('val') ? ""
		tweeters.push(tweeterVal)
		message = $(el).find("textarea").val()
		if message == ""
			message = defaultText
		retweeters = $(el).find(".retweet-group").data("retweeters") ? []
		retweeterVal = $(el).find("input[name='retweet[]']").typeahead('val') ? ""
		retweeters.push(retweeterVal)
		tweets.push
			tweeters: tweeters
			message: message
			retweeters: retweeters
	$("#serializedData").val JSON.stringify tweets
	$("form.container").submit()
	#false